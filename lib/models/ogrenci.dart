import 'dart:convert';

class Ogrenci {
  int _id;
  String _isim;
  int _aktif;
  int get id => _id;

  set id(int value) => _id = value;

  String get isim => _isim;

  set isim(String value) => _isim = value;

  int get aktif => _aktif;

  set aktif(int value) => _aktif = value;
  Ogrenci({String isim, int aktif}) {
    this._isim = isim;
    this._aktif = aktif;
  }
  Ogrenci.withID({int id, String isim, int aktif}) {
    this._id = id;
    this._isim = isim;
    this._aktif = aktif;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': _id,
      'isim': _isim,
      'aktif': _aktif,
    };
  }

  static Ogrenci fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Ogrenci.withID(
      id: map['id'],
      isim: map['isim'],
      aktif: map['aktif'],
    );
  }

  String toJson() => json.encode(toMap());

  static Ogrenci fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() => 'Ogrenci(_id: $_id, _isim: $_isim, _aktif: $_aktif)';
}
