import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:veritabani/models/ogrenci.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;

  //Table Name
  String _ogrenciTablo = "ogrenci";

  //Column Names
  String _columnID = "id";
  String _columnIsim = "isim";
  String _columnAktif = "aktif";

  factory DatabaseHelper() {
    if (_databaseHelper == null) _databaseHelper = DatabaseHelper._internal();
    return _databaseHelper;
  }
  DatabaseHelper._internal();
  Future<Database> _getDatabese() async {
    if (_database == null) _database = await _initializeDatabase();
    return _database;
  }

  _initializeDatabase() async {
    Directory klasor = await getApplicationDocumentsDirectory();
    String dbPath = join(klasor.path, "ogrenci.db");
    debugPrint("DB PATH : " + dbPath);
    var ogrenciDb = openDatabase(dbPath, version: 1, onCreate: _createDatabese);
    return ogrenciDb;
  }

  FutureOr<void> _createDatabese(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $_ogrenciTablo ($_columnID INTEGER PRIMARY KEY AUTOINCREMENT, $_columnIsim TEXT, $_columnAktif INTEGER )");
  }

  //CRUD
  Future<int> ogrenciEkle(Ogrenci ogrenci) async {
    var db = await _getDatabese();
    var sonuc = await db.insert(_ogrenciTablo, ogrenci.toMap(),
        nullColumnHack: "$_columnID");
    debugPrint("Öğrenci eklendi : " + sonuc.toString());
    return sonuc;
  }

  Future<List<Map<String, dynamic>>> tumOgrenciler() async {
    var db = await _getDatabese();
    var sonuc = await db.query(_ogrenciTablo, orderBy: "$_columnID DESC");
    return sonuc;
  }

  Future<int> ogrenciGuncelle(Ogrenci ogrenci) async {
    var db = await _getDatabese();
    var sonuc = await db.update(_ogrenciTablo, ogrenci.toMap(),
        where: "$_columnID = ?", whereArgs: [ogrenci.id]);
    return sonuc;
  }

  Future<int> ogrenciSil(int id) async {
    var db = await _getDatabese();
    var sonuc = await db
        .delete(_ogrenciTablo, where: "$_columnID = ?", whereArgs: [id]);
    return sonuc;
  }

  Future<int> tumOgrenciTablosunuSil() async {
    var db = await _getDatabese();
    var sonuc = await db.delete(_ogrenciTablo);
    return sonuc;
  }
}
