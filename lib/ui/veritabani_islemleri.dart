import 'package:flutter/material.dart';
import 'package:veritabani/models/ogrenci.dart';
import 'package:veritabani/utils/database_helper.dart';

class VeritabaniIslemleri extends StatefulWidget {
  VeritabaniIslemleri({Key key}) : super(key: key);

  @override
  _VeritabaniIslemleriState createState() => _VeritabaniIslemleriState();
}

class _VeritabaniIslemleriState extends State<VeritabaniIslemleri> {
  DatabaseHelper _dbHelper;
  List<Ogrenci> ogrenciler;
  var _formKey = GlobalKey<FormState>();
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _aktiflik = true;
  var _controller = TextEditingController();
  int _selectedIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ogrenciler = List<Ogrenci>();
    _dbHelper = DatabaseHelper();
    _dbHelper.tumOgrenciler().then((value) {
      setState(() {
        value.forEach((element) {
          ogrenciler.add(Ogrenci.fromMap(element));
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Veritabani İşlemleri"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: _controller,
                      validator: (value) {
                        if (value.length < 3)
                          return "En az 3 karekter olmalı";
                        else
                          return null;
                      },
                      decoration: InputDecoration(
                        labelText: "Ogrenci ismini giriniz",
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  SwitchListTile(
                    title: Text("Aktif"),
                    value: _aktiflik,
                    onChanged: (value) {
                      setState(() {
                        _aktiflik = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      _ogrenciEkle(Ogrenci(
                          isim: _controller.text, aktif: _aktiflik ? 1 : 0));
                    }
                  },
                  child: Text("Kaydet"),
                  color: Colors.green,
                ),
                RaisedButton(
                  onPressed: _selectedIndex != null
                      ? () {
                          if (_formKey.currentState.validate()) {
                            _ogrenciGuncelle(
                                Ogrenci.withID(
                                    id: ogrenciler[_selectedIndex].id,
                                    aktif: _aktiflik ? 1 : 0,
                                    isim: _controller.text),
                                _selectedIndex);
                          }
                        }
                      : null,
                  child: Text("Güncelle"),
                  color: Colors.yellow,
                ),
                RaisedButton(
                  onPressed: _tumTabloyuTemizle,
                  child: Text("Tüm Tabloyu Sil"),
                  color: Colors.red,
                ),
              ],
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: ogrenciler.length,
                  itemBuilder: (context, index) {
                    return Card(
                      color: ogrenciler[index].aktif == 1
                          ? Colors.green.shade200
                          : Colors.red.shade200,
                      child: ListTile(
                        onTap: () {
                          setState(() {
                            _controller.text = ogrenciler[index].isim;
                            _aktiflik =
                                ogrenciler[index].aktif == 1 ? true : false;
                            _selectedIndex = index;
                          });
                        },
                        title: Text(ogrenciler[index].isim),
                        subtitle: Text(ogrenciler[index].id.toString()),
                        trailing: IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: () {
                              _ogrenciSil(ogrenciler[index].id, index);
                            }),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }

  void _ogrenciEkle(Ogrenci ogrenci) async {
    var id = await _dbHelper.ogrenciEkle(ogrenci);
    if (id > 0) {
      ogrenci.id = id;
      setState(() {
        ogrenciler.insert(0, ogrenci);
        //_controller.clear();
      });
    }
    _selectedIndex = null;
  }

  void _tumTabloyuTemizle() async {
    var efectedRow = await _dbHelper.tumOgrenciTablosunuSil();
    if (efectedRow > 0) {
      _showSnackBar(efectedRow.toString() + " eleman silindi");
      setState(() {
        ogrenciler.clear();
      });
    }
    _selectedIndex = null;
  }

  void _ogrenciSil(int id, index) async {
    var efecstedRow = await _dbHelper.ogrenciSil(id);
    if (efecstedRow > 0) {
      _showSnackBar(ogrenciler[index].isim + " silindi");
      setState(() {
        ogrenciler.removeAt(index);
      });
    } else
      _showSnackBar("Silme işlemi başarısız");
    _selectedIndex = null;
  }

  void _showSnackBar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      duration: Duration(seconds: 2),
    ));
  }

  void _ogrenciGuncelle(Ogrenci ogrenci, index) async {
    var efectedRow = await _dbHelper.ogrenciGuncelle(ogrenci);
    if (efectedRow > 0) {
      _showSnackBar("Güncelleme işlemi başarılı");
      setState(() {
        ogrenciler[index] = ogrenci;
        _selectedIndex = null;
      });
    } else
      _showSnackBar("Güncelleme işlemi başarısız oldu");
  }
}
